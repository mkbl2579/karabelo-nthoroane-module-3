import 'package:flutter/material.dart';

class Event extends StatefulWidget {
  @override
  _EventState createState() => _EventState();
}

class _EventState extends State<Event> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text('Events Near Me'),
        elevation: .1,
        backgroundColor: Colors.blue,
        ),
      
      
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
        child: GridView.count(
          crossAxisCount: 1,
          padding: EdgeInsets.all((3.0)),
          children: <Widget> [
            makeEventItem("Comedy Night", Icons.mic),
            makeEventItem("Wine Tasting", Icons.local_drink),
            makeEventItem("Braai", Icons.fireplace),
            makeEventItem("Explore", Icons.explore)
          ],
        ),

        
        

      ),
    );
  }
}

Card makeEventItem(String title, IconData icon) {
    return Card(
        elevation: 1.0,
        margin: EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(color: Colors.blue),
          child:  InkWell(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                SizedBox(height: 50.0),
                Center(
                    child: Icon(
                  icon,
                  size: 40.0,
                  color: Colors.white,
                )),
                SizedBox(height: 20.0),
                 Center(
                  child:  Text(title,
                      style:
                           TextStyle(fontSize: 18.0, color: Colors.white)),
                )
              ],
            ),

          )));}