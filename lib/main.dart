import 'package:flutter/material.dart';
import 'package:prototype/login.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    // set dashboard as homepage
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Login(),
    );
  }
}