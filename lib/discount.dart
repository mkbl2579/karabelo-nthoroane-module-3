import 'package:flutter/material.dart';

class Discount extends StatefulWidget {
  @override
  _DiscountState createState() => _DiscountState();
}

class _DiscountState extends State<Discount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text('Discounts Near Me'),
        elevation: .1,
        backgroundColor: Colors.blue,
        ),
      
      
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
        child: GridView.count(
          crossAxisCount: 1,
          padding: EdgeInsets.all((3.0)),
          children: <Widget> [
            makeDiscountItem("Comedy Night", Icons.mic),
            makeDiscountItem("Wine Tasting", Icons.local_drink),
            makeDiscountItem("Braai", Icons.fireplace),
            makeDiscountItem("Explore", Icons.explore)
          ],
        ),

        
        

      ),
    );
  }
}

Card makeDiscountItem(String title, IconData icon) {
    return Card(
        elevation: 1.0,
        margin: EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(color: Colors.blue),
          child:  InkWell(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                SizedBox(height: 50.0),
                Center(
                    child: Icon(
                  icon,
                  size: 40.0,
                  color: Colors.white,
                )),
                SizedBox(height: 20.0),
                 Center(
                  child:  Text(title,
                      style:
                           TextStyle(fontSize: 18.0, color: Colors.white)),
                )
              ],
            ),

          )));}